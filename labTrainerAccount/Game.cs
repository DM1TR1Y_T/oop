﻿using System;

namespace labTrainerAccount
{
    internal class Game
    {

        private Random rnd = new Random();

        public int CountCorrect { get; private set; }
        
        public int CountWrong { get; private set; }
        public string CodeText { get; private set; }
        public string ComplexityGame { get; private set; }

        private bool answerCorrect;

        public event EventHandler Change;

        private enum Complexity { Easily = 20, Medium = 40, Hard = 60 };

        public void DoReset()
        {
            CountCorrect = 0;
            CountWrong = 0;
            DoContinue();
        }

        public void DoSkip()
        {
            DoContinue();
        }

        private void DoContinue()
        {
            //CodeText = "15 + 14 = 20"; //<-- TODO
            //answerCorrect = false;

            double xValue1;
            double xValue2;

            switch (CountCorrect)
            {
                case > 6:
                    xValue1 = rnd.Next((int)Complexity.Hard);
                    xValue2 = rnd.Next((int)Complexity.Hard);
                    ComplexityGame = "Сложно";
                    break;
                case > 3:
                    xValue1 = rnd.Next((int)Complexity.Medium);
                    xValue2 = rnd.Next((int)Complexity.Medium);
                    ComplexityGame = "Средне";
                    break;
                default:
                    xValue1 = rnd.Next((int)Complexity.Easily);
                    xValue2 = rnd.Next((int)Complexity.Easily);
                    ComplexityGame = "Легко";
                    break;
            }

            double xResult;
            char mathOperation;

            switch (rnd.Next(1, 5))
            {
                case 1:
                    xResult = xValue1 + xValue2;
                    mathOperation = '+';
                    break;
                case 2:
                    xResult = xValue1 - xValue2;
                    mathOperation = '-';
                    break;
                case 3:
                    xResult = xValue1 * xValue2;
                    mathOperation = '*';
                    break;
                case 4:
                    xResult = xValue1 / xValue2;
                    mathOperation = '/';
                    break;
                default:
                    xResult = xValue1 + xValue2;
                    mathOperation = '+';
                    break;
            }

            CodeText = $"{xValue1} {mathOperation} {xValue2}";

            int numbOfOperations = 1;

            while (rnd.Next(2) == 1 && numbOfOperations != 3)
            {
                switch (CountCorrect)
                {
                    case > 6:
                        xValue1 = rnd.Next((int)Complexity.Hard);
                        ComplexityGame = "Сложно";
                        break;
                    case > 3:
                        xValue1 = rnd.Next((int)Complexity.Medium);
                        ComplexityGame = "Средне";
                        break;
                    default:
                        xValue1 = rnd.Next((int)Complexity.Easily);
                        ComplexityGame = "Легко";
                        break;
                }

                switch (rnd.Next(1, 4))
                {
                    case 1:
                        xResult += xValue1;
                        mathOperation = '+';
                        break;
                    case 2:
                        xResult -= xValue1;
                        mathOperation = '-';
                        break;
                    case 3:
                        xResult *= xValue1;
                        mathOperation = '*';
                        break;
                    case 4:
                        xResult /= xValue1;
                        mathOperation = '/';
                        break;
                    default:
                        xResult += xValue1;
                        mathOperation = '+';
                        break;
                }

                numbOfOperations++;
                CodeText += $" {mathOperation} {xValue1}";
            }

            double xResultNew = xResult;

            if (rnd.Next(2) == 1)
            {
                xResultNew += rnd.Next(1, 7) * (rnd.Next(2) == 1 ? 1 : -1);
            }

            CodeText += $" = {xResultNew}";
            answerCorrect = xResult == xResultNew;

            Change?.Invoke(this, EventArgs.Empty);

        }

        public void DoAnswer(bool v)
        {
            if (v == answerCorrect)
            {
                CountCorrect++;
            }
            else
            {
                CountWrong++;
            }
            DoContinue();
        }
    }
}