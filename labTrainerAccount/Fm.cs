﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labTrainerAccount
{
    public partial class Fm : Form
    {
        private readonly Game g;

        public Fm()
        {
            InitializeComponent();

            g = new Game();
            g.Change += G_Change;
            g.DoReset();

            buYes.Click += (s, e) => g.DoAnswer(true);
            buNo.Click += (s, e) => g.DoAnswer(false);
            buSkip.Click += (s, e) => g.DoSkip();

            //HW:
            // уровни сложности до 20, до 40, до 60 ... (создать enum) или повышать уровень сложности после 3 правильных ответов пользователя
            // +, -, * сделать разные мат операции в задачах
            // показать уровень сложности пользователю
            // ___ + ___ + ____ = ______ можно попробовать сделать задачи с несколькими значениями
            // добавить кнопку "не знаю" или "пропустить" 
            
        }

        private void G_Change(object sender, EventArgs e)
        {
            laCorrect.Text = $"Верно = {g.CountCorrect}";
            laWrong.Text = $"Неверно = {g.CountWrong}";
            laComplexity.Text = $"Сложность: {g.ComplexityGame}";
            laCode.Text = g.CodeText;
        }
    }
}
