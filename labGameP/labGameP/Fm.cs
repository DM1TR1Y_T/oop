﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labGameP
{
    public partial class Fm : Form
    {
        private int width = 4;
        private int height = 4;
        private bool isGameOver = false;
        private Random rand = new Random();
        Button[,] gamefield;
        public Fm()
        {
            InitializeComponent();
            gamefield = new Button[height, width];
            resetGameField();
            mixGameField();
            this.Resize += Fm_Resize;
        }

        private void Fm_Resize(object sender, EventArgs e)
        {
            for(int h = 0; h < height; h++)
            {
                for(int w = 0; w < width; w++)
                {
                    if (gamefield[h, w] != null)
                    {
                        gamefield[h, w].Width = this.ClientSize.Width / width;
                        gamefield[h, w].Height = this.ClientSize.Height / height;
                        gamefield[h, w].Location = new Point(w * this.ClientSize.Width / width, h * this.ClientSize.Height / height);
                    }
                }
            }
        }

        private void mixGameField()
        {
            for(int h = 0; h < height; h++)
            {
                for(int w = 0; w < width; w++)
                {
                    int he = rand.Next(4);
                    int wi = rand.Next(4);
                    Button bu = gamefield[h, w];
                    gamefield[h, w] = gamefield[he, wi];
                    gamefield[he, wi] = bu;
                }
            }
            updatePos();
        }

        private void resetGameField()
        {
            int cnt = 1;
            for(int h = 0; h < height; h++)
            {
                for(int w = 0; w < width; w++)
                {
                    if (!(h == height - 1) || (w != width - 1))
                    {
                        Button b = new Button();
                        b.Text = cnt.ToString();
                        b.Width = this.ClientSize.Width / width;
                        b.Height = this.ClientSize.Height / height;
                        b.Location = new Point(w * this.ClientSize.Width / width, h * this.ClientSize.Height / height);
                        b.Font = new Font(b.Font.FontFamily, 24, b.Font.Style);
                        b.Click += B_Click;
                        gamefield[h, w] = b;
                        Controls.Add(gamefield[h, w]);
                        cnt++;
                    }
                }
            }
        }

        private void B_Click(object sender, EventArgs e)
        {
            if (!isGameOver)
            {
                if(sender is Button b)
                {
                    int posH, posW;
                    int[] arr = getElemPos(b);
                    posH = arr[0];
                    posW = arr[1];
                    if(posH - 1 >= 0)
                    {
                        if (gamefield[posH-1, posW] == null)
                        {
                            gamefield[posH - 1, posW] = gamefield[posH, posW];
                            gamefield[posH, posW] = null;
                            updatePos();
                            checkGame();
                            return;
                        }
                    }
                    if(posH + 1 < height)
                    {
                        if (gamefield[posH + 1, posW] == null)
                        {
                            gamefield[posH + 1, posW] = gamefield[posH, posW];
                            gamefield[posH, posW] = null;
                            updatePos();
                            checkGame();
                            return;
                        }
                    }
                    if (posW - 1 >= 0)
                    {
                        if (gamefield[posH, posW - 1] == null)
                        {
                            gamefield[posH, posW - 1] = gamefield[posH, posW];
                            gamefield[posH, posW] = null;
                            updatePos();
                            checkGame();
                            return;
                        }
                    }
                    if (posW + 1 < width)
                    {
                        if (gamefield[posH, posW + 1] == null)
                        {
                            gamefield[posH, posW + 1] = gamefield[posH, posW];
                            gamefield[posH, posW] = null;
                            updatePos();
                            checkGame();
                            return;
                        }
                    }
                }
            }
        }

        private void updatePos()
        {
            for (int h = 0; h < height; h++)
            {
                for (int w = 0; w < width; w++)
                {
                    if (gamefield[h, w] != null)
                    {
                        gamefield[h, w].Width = this.ClientSize.Width / width;
                        gamefield[h, w].Height = this.ClientSize.Height / height;
                        gamefield[h, w].Location = new Point(w * this.ClientSize.Width / width, h * this.ClientSize.Height / height);
                    }
                }
            }
        }

        private int[] getElemPos(Button b)
        {
            for(int h = 0; h < height; h++)
            {
                for(int w = 0; w < width; w++)
                {
                    if(gamefield[h,w] != null)
                    {
                        if(gamefield[h,w].Text == b.Text)
                        {
                            return new int[] {h,w};
                        }
                    }
                }
            }
            return new int[] {-1,-1};
        }

        private void checkGame()
        {
            int cnt = 1;
            bool isRight = true;
            for(int h = 0; h < height; h++)
            {
                for(int w = 0; w < width; w++)
                {
                    if ((gamefield[h, w] != null))
                    {
                        if (!(gamefield[h, w].Text == cnt.ToString()))
                        {
                            isRight = false;
                            break;
                        }
                        cnt++;
                    }
                    else if (h == height - 1 && w == width - 1)
                    {
                        isRight = true;
                    }
                    else
                    {
                        isRight = false;
                        break;
                    }
                }
                if (!isRight)
                    break;
            }
            if(isRight)
            {
                label1.Text = "Вы победили!";
                label1.Font = new Font(label1.Font.FontFamily, 32, FontStyle.Bold);
                label1.Size = new Size(label1.PreferredWidth, label1.PreferredHeight);
                label1.BringToFront();
                isGameOver = true;
            }
        }
    }
}
