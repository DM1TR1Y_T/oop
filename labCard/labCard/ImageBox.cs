﻿using System;
using System.Drawing;

namespace labCard {
    internal class ImageBox {
        private Bitmap[] images;

        public int Rows { get; }
        public int Cols { get; }
        public int Count { get; }

        public ImageBox(Bitmap image, int rows, int cols) : this(image, rows, cols, rows * cols) { }
        public ImageBox(Bitmap image, int rows, int cols, int count) {
            Rows = rows;
            Cols = cols;
            Count = count;
            LoadImages(image);
        }

        public Bitmap this[int index] { get { return images[index]; } } // индексатор

        private void LoadImages(Bitmap image) {
            var b = new Bitmap(image);
            var w = b.Width / Cols;
            var h = b.Height / Rows;
            var n = 0;

            images = new Bitmap[Count];
            for (int i = 0; i < Rows; i++) {
                for (int j = 0; j < Cols && n < Count; j++, n++) {
                    images[n] = new Bitmap(w, h);
                    
                    // (1)
                    /*
                    var g = Graphics.FromImage(images[n]);
                    g.DrawImage(b, 0, 0, new Rectangle(j * w, i * h, w, h), GraphicsUnit.Pixel);
                    g.Dispose();
                    */
                    // (2)
                    using (var g = Graphics.FromImage(images[n])) {
                        g.DrawImage(b, 0, 0, new Rectangle(j * w, i * h, w, h), GraphicsUnit.Pixel);
                    }

                }
            }

        }
    }
}