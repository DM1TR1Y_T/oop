﻿using System;
using System.Linq;

namespace labCard {
    internal class CardDeck {

        private Random rnd = new Random();
        private int count;
        private int[] cards;
        public int Count { get; }

        public CardDeck(int count) {
            Count = count;
            CardRandom();
        }


        public void CardSort() => cards = Enumerable.Range(0, Count).ToArray();
        public void CardRandom() => cards = Enumerable.Range(0, Count).OrderBy(_ => rnd.Next()).ToArray();
    
        public int this[int index] {get { return cards[index]; } }
    }
}