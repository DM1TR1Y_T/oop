﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labControlOnTLP
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            var list = new List<PictureBox>
            {
                pictureBox1,
                pictureBox2,
                pictureBox3,
                pictureBox4,
                pictureBox5,
                pictureBox6,
                pictureBox7,
                pictureBox8,
                pictureBox9,
            };

            //HW --> пробежаться по TLP
            //Controls
            int n = 1;
            foreach (var item in list)
            {
                item.MouseEnter += PictureBoxAll_MouseEnter;
                item.MouseLeave += PictureBoxAll_MouseLeave;
                item.Click += PictureBoxAll_Click;
                item.Image = imageList1.Images[0];
                item.Tag = n++;
                item.SizeMode = PictureBoxSizeMode.Zoom;
            }
        }

        private void PictureBoxAll_Click(object sender, EventArgs e)
        {
            if (sender is PictureBox x)
                x.BackColor = x.BackColor == SystemColors.Control ? Color.Green : SystemColors.Control;
        }

        private void PictureBoxAll_MouseLeave(object sender, EventArgs e)
        {
            if (sender is PictureBox x)
                x.Image = imageList1.Images[0];
        }

        private void PictureBoxAll_MouseEnter(object sender, EventArgs e)
        {
            if (sender is PictureBox x)
                x.Image = imageList1.Images[Convert.ToInt32(x.Tag)];
        }
    }

}
