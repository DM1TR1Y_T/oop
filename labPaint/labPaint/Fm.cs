﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
// TODO: hw - аналог paint
// добавить больше фигур (ромб, шестиугольник, стрелки)
// режимы заливки

// добавить пипетку [GetPixel(...)]
// выделить область мышкой 
// перемещение выделенной области g.DrawImage(...)
// добавить повороты (выделенной области)
// механизм изменения размера холста [b] ( в месте инициализации)

// получить картинку из буфера обмена
// доделать заливку


namespace labPaint {
    public partial class Fm : Form {

        private Bitmap map;
        private Graphics g;
        private Point startMouseDown;
        private Bitmap startMap;
        private Pen myPen;
        private MyDrawMode myDrawMode = MyDrawMode.Pencil; // режим по умолчанию
        private Color currentColor;

        enum MyDrawMode { // перечисление типов рисования
            Pencil, Line, Ellipse, Rectangle,
            Romb, Triangle, Selected, // выделение
            Pipet
        }

        public Fm() {
            InitializeComponent();

            map = new Bitmap(Screen.PrimaryScreen.Bounds.Width, 
                             Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(map);
            myPen = new Pen(color1.BackColor, 5);
            // добавить скругление линиям
            myPen.StartCap = myPen.EndCap = System.Drawing.Drawing2D.LineCap.Round;
            // качество отрисовки
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            pxImage.MouseDown += PxImage_MouseDown;
            pxImage.MouseMove += PxImage_MouseMove;
            pxImage.MouseUp += PxImage_MouseUp;
            pxImage.Paint += (s, e) => e.Graphics.DrawImage(map, 0, 0);


            color3.Click += (s, e) => myPen.Color = color3.BackColor;
            color1.Click += (s, e) => myPen.Color = color1.BackColor;
            color4.Click += (s, e) => myPen.Color = color4.BackColor;
            color2.Click += (s, e) => myPen.Color = color2.BackColor;

            buPipet.Click += (s, e) => myDrawMode = MyDrawMode.Pipet;
            rubber.Click += (s, e) => myPen.Color = pxImage.BackColor;

            changeColor.Click += (s, e) => {
                if (chooseColor.ShowDialog() == DialogResult.OK) {
                    if (myPen.Color != chooseColor.Color) {
                        swapColor();
                    }

                }
            };

            //trackBar1.Value = (int) myPen.Width;
            trackBar1.Value = 5;
            sizeLabel.Text = $"Size: {trackBar1.Value}";
            trackBar1.ValueChanged += (s, e) => {
                myPen.Width = trackBar1.Value;
                sizeLabel.Text = $"Size: {trackBar1.Value}";
            };



            buImageClear.Click += BuImageClear_Click;
            buSaveToFile.Click += BuSaveToFile_Click;
            buLoadFromFile.Click += BuLoadFromFile_Click;
            buClipboardCopy.Click += (s, e) => Clipboard.SetImage(map);
           

            buDrawPensil.Click += (s, e) => myDrawMode = MyDrawMode.Pencil;
            buDrawLine.Click += (s, e) => myDrawMode = MyDrawMode.Line;
            duDrawEllipce.Click += (s, e) => myDrawMode = MyDrawMode.Ellipse;
            duDrawRectangle.Click += (s, e) => myDrawMode = MyDrawMode.Rectangle;
            buDrawTriangle.Click += (s, e) => myDrawMode = MyDrawMode.Triangle;
            buDrawRomb.Click += (s, e) => myDrawMode = MyDrawMode.Romb;
        }

        private void swapColor() {
            if (color1.BackColor != myPen.Color) {
                if (color2.BackColor != myPen.Color) {
                    color4.BackColor = color3.BackColor;
                    color3.BackColor = color2.BackColor;
                    color2.BackColor = color1.BackColor;
                    color1.BackColor = myPen.Color;
                }
                else {
                    color2.BackColor = color1.BackColor;
                    color1.BackColor = myPen.Color;
                }
            }      
        }

        private void BuLoadFromFile_Click(object sender, EventArgs e) {

            OpenFileDialog dialog = new OpenFileDialog();
            //dialog.Filter = "Image Files"; // фильтры в диалоговом окне
            if (dialog.ShowDialog() == DialogResult.OK) {
                g.Clear(pxImage.BackColor);
                g.DrawImage(Bitmap.FromFile(dialog.FileName), 0, 0);
                pxImage.Invalidate();
            }
        }

        private void BuSaveToFile_Click(object sender, EventArgs e) {
            SaveFileDialog dialog = new SaveFileDialog();
            //dialog.Filter = ""; // фильтр
            if (dialog.ShowDialog() == DialogResult.OK) {
                map.Save(dialog.FileName);
            }
        }

        private void BuImageClear_Click(object sender, EventArgs e) {
            g.Clear(pxImage.BackColor);
            pxImage.Invalidate();
        }

        private void PxImage_MouseUp(object sender, MouseEventArgs e) {
            if (myDrawMode == MyDrawMode.Line) {

            }
        }

        private void PxImage_MouseMove(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) {
                int h = (e.Y - startMouseDown.Y) > 0 ? e.Y - startMouseDown.Y: e.Y - startMouseDown.Y;
                int w = e.X - startMouseDown.X;

                var p1 = new Point(Math.Min(startMouseDown.X, e.X), Math.Min(startMouseDown.Y, e.Y));
                var p2 = new Point(Math.Max(startMouseDown.X, e.X), Math.Max(startMouseDown.Y, e.Y));

                switch (myDrawMode) {
                    case MyDrawMode.Pencil:
                        g.DrawLine(myPen, startMouseDown, e.Location);
                        startMouseDown = e.Location;
                        break;

                    case MyDrawMode.Line:
                        clearBitmap();
                        g.DrawLine(myPen, startMouseDown, e.Location);
                        break;
                     
                    case MyDrawMode.Ellipse:
                        clearBitmap();
                        if (isFill.Checked) {
                            // заливка фигуры
                            g.FillEllipse(new SolidBrush(myPen.Color), 
                                startMouseDown.X, startMouseDown.Y, w, h);
                        }
                        else {
                            g.DrawEllipse(myPen, startMouseDown.X, startMouseDown.Y, w, h);
                        }
                        break;

                    case MyDrawMode.Rectangle:
                        clearBitmap();
                        if (isFill.Checked) {
                            g.FillRectangle(new SolidBrush(myPen.Color), p1.X, p1.Y, p2.X - p1.X, p2.Y - p1.Y);
                        }
                        else {
                            g.DrawRectangle(myPen, p1.X, p1.Y, p2.X - p1.X, p2.Y - p1.Y);
                        }
                        break;

                    case MyDrawMode.Triangle:
                        clearBitmap();
                        var haftX = (p2.X - p1.X) / 2;
                        g.DrawLine(myPen, p1.X, p2.Y, p1.X + haftX, p1.Y);
                        g.DrawLine(myPen, p1.X + haftX, p1.Y, p2.X, p2.Y);
                        g.DrawLine(myPen, p1.X, p2.Y, p2.X, p2.Y);
                        


                      //  var pT1 = new Point(1, 1);
                       // var pT2 = new Point(1, 1);
                        //var pointList = new Point[] { pT1, pT2 }; 
                        //g.DrawPolygon(____, pointList);

                        break;

                    case MyDrawMode.Romb:
                        break;

             

                    default:
                        break;

                }
            }
            else if(e.Button == MouseButtons.Right){
                currentColor = myPen.Color;
                myPen.Color = pxImage.BackColor;
                g.DrawLine(myPen, startMouseDown, e.Location);
                myPen.Color = currentColor;
                startMouseDown = e.Location;
            }
            pxImage.Invalidate(); // запускает перересовку компонента (событие paint)
        }

        private void clearBitmap() {
            g.Dispose();
            map = (Bitmap)startMap.Clone();
            g = Graphics.FromImage(map);
        }

        private void PxImage_MouseDown(object sender, MouseEventArgs e) {

            startMouseDown = e.Location;
            startMap = (Bitmap)map.Clone(); // делаю копию битмапа 

             if (myDrawMode == MyDrawMode.Pipet) {
                myPen.Color = startMap.GetPixel(e.X, e.Y);
                swapColor();
            }
        }

        private void pxImage_Click(object sender, EventArgs e) {

        }
    }


}
