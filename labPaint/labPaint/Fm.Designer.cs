﻿
namespace labPaint {
    partial class Fm {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panel1 = new System.Windows.Forms.Panel();
            this.isFill = new System.Windows.Forms.CheckBox();
            this.buDrawRomb = new System.Windows.Forms.Button();
            this.buDrawTriangle = new System.Windows.Forms.Button();
            this.sizeLabel = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.color1 = new System.Windows.Forms.PictureBox();
            this.color2 = new System.Windows.Forms.PictureBox();
            this.color3 = new System.Windows.Forms.PictureBox();
            this.color4 = new System.Windows.Forms.PictureBox();
            this.rubber = new System.Windows.Forms.Button();
            this.changeColor = new System.Windows.Forms.Button();
            this.duDrawRectangle = new System.Windows.Forms.Button();
            this.duDrawEllipce = new System.Windows.Forms.Button();
            this.buDrawLine = new System.Windows.Forms.Button();
            this.buDrawPensil = new System.Windows.Forms.Button();
            this.buClipboardCopy = new System.Windows.Forms.Button();
            this.buLoadFromFile = new System.Windows.Forms.Button();
            this.buSaveToFile = new System.Windows.Forms.Button();
            this.buImageClear = new System.Windows.Forms.Button();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.pxImage = new System.Windows.Forms.PictureBox();
            this.chooseColor = new System.Windows.Forms.ColorDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.buPipet = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.color1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.color2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.color3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.color4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.isFill);
            this.panel1.Controls.Add(this.buDrawRomb);
            this.panel1.Controls.Add(this.buDrawTriangle);
            this.panel1.Controls.Add(this.sizeLabel);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Controls.Add(this.duDrawRectangle);
            this.panel1.Controls.Add(this.duDrawEllipce);
            this.panel1.Controls.Add(this.buDrawLine);
            this.panel1.Controls.Add(this.buDrawPensil);
            this.panel1.Controls.Add(this.buClipboardCopy);
            this.panel1.Controls.Add(this.buLoadFromFile);
            this.panel1.Controls.Add(this.buSaveToFile);
            this.panel1.Controls.Add(this.buImageClear);
            this.panel1.Controls.Add(this.trackBar1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 598);
            this.panel1.TabIndex = 0;
            // 
            // isFill
            // 
            this.isFill.AutoSize = true;
            this.isFill.Location = new System.Drawing.Point(37, 514);
            this.isFill.Name = "isFill";
            this.isFill.Size = new System.Drawing.Size(50, 24);
            this.isFill.TabIndex = 17;
            this.isFill.Text = "Fill";
            this.isFill.UseVisualStyleBackColor = true;
            // 
            // buDrawRomb
            // 
            this.buDrawRomb.Location = new System.Drawing.Point(112, 446);
            this.buDrawRomb.Name = "buDrawRomb";
            this.buDrawRomb.Size = new System.Drawing.Size(94, 29);
            this.buDrawRomb.TabIndex = 16;
            this.buDrawRomb.Text = "Romb";
            this.buDrawRomb.UseVisualStyleBackColor = true;
            // 
            // buDrawTriangle
            // 
            this.buDrawTriangle.Location = new System.Drawing.Point(15, 446);
            this.buDrawTriangle.Name = "buDrawTriangle";
            this.buDrawTriangle.Size = new System.Drawing.Size(94, 29);
            this.buDrawTriangle.TabIndex = 15;
            this.buDrawTriangle.Text = "Triangle";
            this.buDrawTriangle.UseVisualStyleBackColor = true;
            // 
            // sizeLabel
            // 
            this.sizeLabel.AutoSize = true;
            this.sizeLabel.Location = new System.Drawing.Point(93, 186);
            this.sizeLabel.Name = "sizeLabel";
            this.sizeLabel.Size = new System.Drawing.Size(36, 20);
            this.sizeLabel.TabIndex = 14;
            this.sizeLabel.Text = "Size";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.color1);
            this.flowLayoutPanel1.Controls.Add(this.color2);
            this.flowLayoutPanel1.Controls.Add(this.color3);
            this.flowLayoutPanel1.Controls.Add(this.color4);
            this.flowLayoutPanel1.Controls.Add(this.buPipet);
            this.flowLayoutPanel1.Controls.Add(this.rubber);
            this.flowLayoutPanel1.Controls.Add(this.changeColor);
            this.flowLayoutPanel1.Controls.Add(this.button1);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(212, 121);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // color1
            // 
            this.color1.BackColor = System.Drawing.Color.Red;
            this.color1.Location = new System.Drawing.Point(3, 3);
            this.color1.Name = "color1";
            this.color1.Size = new System.Drawing.Size(47, 42);
            this.color1.TabIndex = 1;
            this.color1.TabStop = false;
            // 
            // color2
            // 
            this.color2.BackColor = System.Drawing.SystemColors.Desktop;
            this.color2.Location = new System.Drawing.Point(56, 3);
            this.color2.Name = "color2";
            this.color2.Size = new System.Drawing.Size(47, 42);
            this.color2.TabIndex = 4;
            this.color2.TabStop = false;
            // 
            // color3
            // 
            this.color3.BackColor = System.Drawing.Color.LawnGreen;
            this.color3.Location = new System.Drawing.Point(109, 3);
            this.color3.Name = "color3";
            this.color3.Size = new System.Drawing.Size(47, 42);
            this.color3.TabIndex = 2;
            this.color3.TabStop = false;
            // 
            // color4
            // 
            this.color4.BackColor = System.Drawing.Color.Blue;
            this.color4.Location = new System.Drawing.Point(162, 3);
            this.color4.Name = "color4";
            this.color4.Size = new System.Drawing.Size(47, 42);
            this.color4.TabIndex = 3;
            this.color4.TabStop = false;
            // 
            // rubber
            // 
            this.rubber.Location = new System.Drawing.Point(109, 51);
            this.rubber.Name = "rubber";
            this.rubber.Size = new System.Drawing.Size(100, 32);
            this.rubber.TabIndex = 15;
            this.rubber.Text = "Rubber";
            this.rubber.UseVisualStyleBackColor = true;
            // 
            // changeColor
            // 
            this.changeColor.Location = new System.Drawing.Point(3, 89);
            this.changeColor.Name = "changeColor";
            this.changeColor.Size = new System.Drawing.Size(206, 32);
            this.changeColor.TabIndex = 5;
            this.changeColor.Text = "Custom color";
            this.changeColor.UseVisualStyleBackColor = true;
            // 
            // duDrawRectangle
            // 
            this.duDrawRectangle.Location = new System.Drawing.Point(112, 411);
            this.duDrawRectangle.Name = "duDrawRectangle";
            this.duDrawRectangle.Size = new System.Drawing.Size(94, 29);
            this.duDrawRectangle.TabIndex = 13;
            this.duDrawRectangle.Text = "Rectangle";
            this.duDrawRectangle.UseVisualStyleBackColor = true;
            // 
            // duDrawEllipce
            // 
            this.duDrawEllipce.Location = new System.Drawing.Point(12, 411);
            this.duDrawEllipce.Name = "duDrawEllipce";
            this.duDrawEllipce.Size = new System.Drawing.Size(94, 29);
            this.duDrawEllipce.TabIndex = 12;
            this.duDrawEllipce.Text = "Ellipce";
            this.duDrawEllipce.UseVisualStyleBackColor = true;
            // 
            // buDrawLine
            // 
            this.buDrawLine.Location = new System.Drawing.Point(12, 376);
            this.buDrawLine.Name = "buDrawLine";
            this.buDrawLine.Size = new System.Drawing.Size(94, 29);
            this.buDrawLine.TabIndex = 11;
            this.buDrawLine.Text = "Line";
            this.buDrawLine.UseVisualStyleBackColor = true;
            // 
            // buDrawPensil
            // 
            this.buDrawPensil.Location = new System.Drawing.Point(112, 376);
            this.buDrawPensil.Name = "buDrawPensil";
            this.buDrawPensil.Size = new System.Drawing.Size(94, 29);
            this.buDrawPensil.TabIndex = 10;
            this.buDrawPensil.Text = "Pensil";
            this.buDrawPensil.UseVisualStyleBackColor = true;
            // 
            // buClipboardCopy
            // 
            this.buClipboardCopy.Location = new System.Drawing.Point(12, 333);
            this.buClipboardCopy.Name = "buClipboardCopy";
            this.buClipboardCopy.Size = new System.Drawing.Size(206, 29);
            this.buClipboardCopy.TabIndex = 9;
            this.buClipboardCopy.Text = "Copy";
            this.buClipboardCopy.UseVisualStyleBackColor = true;
            // 
            // buLoadFromFile
            // 
            this.buLoadFromFile.Location = new System.Drawing.Point(12, 297);
            this.buLoadFromFile.Name = "buLoadFromFile";
            this.buLoadFromFile.Size = new System.Drawing.Size(206, 29);
            this.buLoadFromFile.TabIndex = 8;
            this.buLoadFromFile.Text = "Load";
            this.buLoadFromFile.UseVisualStyleBackColor = true;
            // 
            // buSaveToFile
            // 
            this.buSaveToFile.Location = new System.Drawing.Point(12, 262);
            this.buSaveToFile.Name = "buSaveToFile";
            this.buSaveToFile.Size = new System.Drawing.Size(206, 29);
            this.buSaveToFile.TabIndex = 7;
            this.buSaveToFile.Text = "Save";
            this.buSaveToFile.UseVisualStyleBackColor = true;
            // 
            // buImageClear
            // 
            this.buImageClear.Location = new System.Drawing.Point(12, 227);
            this.buImageClear.Name = "buImageClear";
            this.buImageClear.Size = new System.Drawing.Size(206, 29);
            this.buImageClear.TabIndex = 6;
            this.buImageClear.Text = "Clear";
            this.buImageClear.UseVisualStyleBackColor = true;
            // 
            // trackBar1
            // 
            this.trackBar1.LargeChange = 1;
            this.trackBar1.Location = new System.Drawing.Point(15, 139);
            this.trackBar1.Maximum = 15;
            this.trackBar1.Minimum = 1;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(206, 56);
            this.trackBar1.TabIndex = 5;
            this.trackBar1.Tag = "";
            this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBar1.Value = 1;
            // 
            // pxImage
            // 
            this.pxImage.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pxImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pxImage.Location = new System.Drawing.Point(250, 0);
            this.pxImage.Name = "pxImage";
            this.pxImage.Size = new System.Drawing.Size(550, 598);
            this.pxImage.TabIndex = 1;
            this.pxImage.TabStop = false;
            this.pxImage.Click += new System.EventHandler(this.pxImage_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(3, 127);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(206, 32);
            this.button1.TabIndex = 16;
            this.button1.Text = "Rubber";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // buPipet
            // 
            this.buPipet.Location = new System.Drawing.Point(3, 51);
            this.buPipet.Name = "buPipet";
            this.buPipet.Size = new System.Drawing.Size(100, 32);
            this.buPipet.TabIndex = 17;
            this.buPipet.Text = "Pipet";
            this.buPipet.UseVisualStyleBackColor = true;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 598);
            this.Controls.Add(this.pxImage);
            this.Controls.Add(this.panel1);
            this.Name = "Fm";
            this.Text = "labPaint";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.color1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.color2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.color3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.color4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox color2;
        private System.Windows.Forms.PictureBox color4;
        private System.Windows.Forms.PictureBox color3;
        private System.Windows.Forms.PictureBox color1;
        private System.Windows.Forms.PictureBox pxImage;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Button buLoadFromFile;
        private System.Windows.Forms.Button buSaveToFile;
        private System.Windows.Forms.Button buImageClear;
        private System.Windows.Forms.Button buClipboardCopy;
        private System.Windows.Forms.Button duDrawRectangle;
        private System.Windows.Forms.Button duDrawEllipce;
        private System.Windows.Forms.Button buDrawLine;
        private System.Windows.Forms.Button buDrawPensil;
        private System.Windows.Forms.ColorDialog chooseColor;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button changeColor;
        private System.Windows.Forms.Label sizeLabel;
        private System.Windows.Forms.Button rubber;
        private System.Windows.Forms.Button buDrawTriangle;
        private System.Windows.Forms.Button buDrawRomb;
        private System.Windows.Forms.CheckBox isFill;
        private System.Windows.Forms.Button buPipet;
        private System.Windows.Forms.Button button1;
    }
}

