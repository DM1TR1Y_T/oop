﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BullsAndCows
{
    public partial class Fm : Form
    {
        private const int numbersAmount = 4;
        private Random random = new Random();
        private string generatedNumber = "";
        public Fm()
        {
            InitializeComponent();
            generatedNumber = generate();
            textBox.MaxLength = numbersAmount;
            guess.Click += Guess_Click;
            restart.Click += Restart_Click;
            lv.View = View.List;
        }

        private void Restart_Click(object sender, EventArgs e)
        {
            winMessage.Text = "";
            guess.Enabled = true;
            generatedNumber = generate();
            lv.Items.Clear();
        }

        private void Guess_Click(object sender, EventArgs e)
        {
            if(checkRight())
            {
                int[] bullsAndCows = check();
                lv.Items.Add(new ListViewItem($"{textBox.Text} : {bullsAndCows[0]} быков, {bullsAndCows[1]} коров"));
                if(bullsAndCows[0] == numbersAmount)
                {
                    guess.Enabled = false;
                    winMessage.Text = $"Поздравляем, вы победили, загаданное число: {generatedNumber}";
                }
                textBox.Text = "";
            }
        }

        private string generate()
        {
            string num = "";
            for (int i = 0; i < numbersAmount; i++)
            {
                int j = random.Next(10);
                while (true)
                {
                    if (!num.Contains(j.ToString()))
                        break;
                    j = random.Next(10);
                }
                num = num + j;
            }
            return num;
        }

        private Boolean checkRight()
        {
            char[] playerGuess = textBox.Text.ToCharArray();
            if (playerGuess.Length < numbersAmount)
                return false;
            for (int i = 0; i < playerGuess.Length; i++)
            {
                if (!Char.IsDigit(playerGuess[i]))
                {
                    return false;
                }
            }
            for (int i = 0; i < playerGuess.Length; i++)
            {
                for(int k = 0; k < playerGuess.Length; k++)
                {
                    if (playerGuess[i] == playerGuess[k] && i != k)
                        return false;
                }
            }
            return true;
        }

        private int[] check()
        {
            char[] playerGuess = textBox.Text.ToCharArray();
            char[] realSym = generatedNumber.ToCharArray();
            int bullsAmount = 0;
            int cowsAmount = 0;
            for(int i = 0; i < numbersAmount; i++)
            {
                if(realSym[i] == playerGuess[i])
                {
                    bullsAmount++;
                    realSym[i] = '@';
                    playerGuess[i] = '@';
                }
            }
            for (int i = 0; i < numbersAmount; i++)
            {
                if(playerGuess[i] != '@')
                {
                    for(int k = 0; k < numbersAmount; k++)
                    {
                        if(realSym[k] == playerGuess[i])
                        {
                            realSym[k] = '@';
                            cowsAmount++;
                            break;
                        }
                    }
                }
            }

            return new int[] {bullsAmount, cowsAmount};
        }
    }
}
