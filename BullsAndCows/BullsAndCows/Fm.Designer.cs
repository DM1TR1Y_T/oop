﻿
namespace BullsAndCows
{
    partial class Fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.restart = new System.Windows.Forms.Button();
            this.textBox = new System.Windows.Forms.TextBox();
            this.guess = new System.Windows.Forms.Button();
            this.lv = new System.Windows.Forms.ListView();
            this.winMessage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // restart
            // 
            this.restart.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.restart.Location = new System.Drawing.Point(192, 62);
            this.restart.Name = "restart";
            this.restart.Size = new System.Drawing.Size(128, 47);
            this.restart.TabIndex = 0;
            this.restart.Text = "Начать заново";
            this.restart.UseVisualStyleBackColor = true;
            // 
            // textBox
            // 
            this.textBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox.Location = new System.Drawing.Point(192, 129);
            this.textBox.MaxLength = 4;
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(128, 20);
            this.textBox.TabIndex = 1;
            // 
            // guess
            // 
            this.guess.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.guess.Location = new System.Drawing.Point(192, 166);
            this.guess.Name = "guess";
            this.guess.Size = new System.Drawing.Size(128, 44);
            this.guess.TabIndex = 2;
            this.guess.Text = "Сделать ход";
            this.guess.UseVisualStyleBackColor = true;
            // 
            // lv
            // 
            this.lv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lv.HideSelection = false;
            this.lv.Location = new System.Drawing.Point(192, 216);
            this.lv.Name = "lv";
            this.lv.Size = new System.Drawing.Size(128, 243);
            this.lv.TabIndex = 3;
            this.lv.UseCompatibleStateImageBehavior = false;
            // 
            // winMessage
            // 
            this.winMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.winMessage.AutoSize = true;
            this.winMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.winMessage.Location = new System.Drawing.Point(12, 32);
            this.winMessage.Name = "winMessage";
            this.winMessage.Size = new System.Drawing.Size(0, 24);
            this.winMessage.TabIndex = 4;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 471);
            this.Controls.Add(this.winMessage);
            this.Controls.Add(this.lv);
            this.Controls.Add(this.guess);
            this.Controls.Add(this.textBox);
            this.Controls.Add(this.restart);
            this.Name = "Fm";
            this.Text = "Быки и коровы";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button restart;
        private System.Windows.Forms.TextBox textBox;
        private System.Windows.Forms.Button guess;
        private System.Windows.Forms.ListView lv;
        private System.Windows.Forms.Label winMessage;
    }
}

